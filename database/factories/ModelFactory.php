<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

// $factory->define(App\User::class, function (Faker\Generator $faker) {
//     return [
//         'firstname' => $faker->name,
//         'lastname' => $faker->lastName,
//         'email' => $faker->unique()->safeEmail,
//         'password' => $password ?: $password = bcrypt('secret'),
//         'remember_token' => str_random(10),
//     ];
// });
//
//
$factory->define(App\Brand::class, function (Faker\Generator $faker) {

    return [
        'brand_name' => $faker->company,
        'brand_website' => $faker->domainName,
        'brand_about' => $faker->realText(150),
        'brand_logo' => $faker->imageUrl
    ];
});

$factory->define(App\Products::class, function (Faker\Generator $faker) {
    return [
        'product_name' => $faker->name,
        'product_about' => $faker->text,
        'product_img' => $faker->imageUrl,
        'brand_id' => $faker->numberBetween(1,30),
        'product_cats_id' => $faker->numberBetween(1,7)
    ];
});

$factory->define(App\Categories::class, function (Faker\Generator $faker) {
    return [
        'category_name' => $faker->name
    ];
});

$factory->define(App\Reviews::class, function (Faker\Generator $faker) {
    return [
        'reviewed_by' => $faker->name,
        'review_text' => $faker->text,
        'products_id' => $faker->numberBetween(1,130),
        'review_time' => $faker->dateTimeBetween('-2 years', 'now', date_default_timezone_get())
    ];
});

$factory->define(App\Ratings::class, function (Faker\Generator $faker) {
    return [
        'rated_by' => $faker->name,
        'rating_star' => $faker->numberBetween(0, 5),
        'brand_id' => $faker->numberBetween(1,30),
        'rating_time' => $faker->dateTimeBetween('-2 years', 'now', date_default_timezone_get())
    ];
});
