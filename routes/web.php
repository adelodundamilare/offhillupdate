<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    $brands = App\Brand::with('rating')->get();

    echo $brands;

    // return view('home', compact('brands'));
});

Route::get('brand', 'BrandController@index');
