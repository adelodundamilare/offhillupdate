<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Offhill.com | @yield('title')</title>

    <!-- Fonts -->
    <link href="{{ elixir("css/fonts/font-awesome.min.css") }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ elixir("css/app.css") }}">
    <!-- <link rel="stylesheet" type="text/css" href="./css/app.css")> -->

    <!-- Styles -->
</head>
<body>

<div id="app">
    <section class="head">
        <div class="top-bar"></div>
        <div class="header-section">
            <div class="wrap">
                <div class="header-bar">
                    <div class="header-bar__logo">
                        <h1 class="header-bar__h1">
                            <a href="/"><img class="header-bar__img" src="./img/offhill-logo.png" alt="Offhill Logo"></a>
                        </h1>
                    </div>
                    <div class="header-bar__mobile-icon">
                        <button @click="showMobileMenus">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="header-bar__head-links">
                        <button class="header-bar__register"><i class="fa fa-user" aria-hidden="true"></i> Register Here</button>
                        <button class="header-bar__sign-in"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign in</button>
                    </div>
                </div>
            </div>

            <div class="nav-bar" v-show="show">
                <nav class="wrap no-padding">
                    <ul class="nav-bar__ul">
                        <li class="nav-bar__li not-mobile"><a class="nav-bar__a" href="#">Register</a></li>
                        <li class="nav-bar__li not-mobile"><a class="nav-bar__a" href="#">Sign In</a></li>
                        <li class="nav-bar__li"><a class="nav-bar__a" href="#">Home</a></li>
                        <li class="nav-bar__li nav-bar__li--has-child" @mouseover="showChild()" @mouseout="!showChild()">
                            <a class="nav-bar__a nav-bar__li--has-child-link" href="#" @click="noDisplay = !showChild()">
                                <span>Sectors</span>
                                <!-- <i class="fa fa-caret-up" aria-hidden="true"></i> -->
                                <i class="fa fa-caret-down" aria-hidden="true"></i>
                            </a>

                            <ul class="nav-bar__inner-ul" :class="{'no-display': noDisplay}">
                                <li class="nav-bar__inner-li"><a class="nav-bar__inner-li__a" href="#">Telecomms</a></li>
                                <li class="nav-bar__inner-li"><a class="nav-bar__inner-li__a" href="#">Travels</a></li>
                                <li class="nav-bar__inner-li"><a class="nav-bar__inner-li__a" href="#">Electronics</a></li>
                                <li class="nav-bar__inner-li"><a class="nav-bar__inner-li__a" href="#">Finance</a></li>
                                <li class="nav-bar__inner-li"><a class="nav-bar__inner-li__a" href="#">Hospitality</a></li>
                                <li class="nav-bar__inner-li"><a class="nav-bar__inner-li__a" href="#">Consumer Goods</a></li>
                                <li class="nav-bar__inner-li"><a class="nav-bar__inner-li__a" href="#">Auto Mobile</a></li>
                            </ul>
                        </li>
                        <li class="nav-bar__li"><a class="nav-bar__a" href="#">About</a></li>
                        <li class="nav-bar__li"><a class="nav-bar__a" href="#">Complaints</a></li>
                        <li class="nav-bar__li"><a class="nav-bar__a" href="#">Blog</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>

    <section class="page-body">

        @yield('body')
    </section>

    <section class="footer">
        <div class="wrap">
            <div class="footer-content">
                <div class="footer-content__box footer-content__box1">
                    <h1 class="footer-content__box1__h1">
                        <a href="/"><img class="footer-content__box1__img" src="./img/offhill-footer-logo.png" alt="Offhill Logo"></a>
                    </h1>
                    <article class="footer-content__box1__article">
                        <a href="/">Offhill.com</a> is the easiest way to file a consumer complaint online. With more than 4,000 companies listed in our database, you can very quickly find and register feedback about an organization. So, the next time you encounter a rude employee, a billing problem, or a company tries to rip you off, visit <a href="/">Offhill.com</a>.
                    </article>
                    <ul class="footer-content__box1__social-icon-ul">
                        <li class="footer-content__box1__social-icon-li"> <a  class="footer-content__box1__social-icon-li-a" href="#"><i class="fa fa-facebook" aria-hidden="true"></a></i></li>
                        <li class="footer-content__box1__social-icon-li"> <a  class="footer-content__box1__social-icon-li-a" href="#"><i class="fa fa-twitter" aria-hidden="true"></a></i></li>
                        <li class="footer-content__box1__social-icon-li"> <a  class="footer-content__box1__social-icon-li-a" href="#"><i class="fa fa-linkedin" aria-hidden="true"></a></i></li>
                    </ul>
                </div>
                <div class="footer-content__box2">
                    <h4 class="footer-content__box2__h4">Sectors</h4>
                    <ul class="footer-content__box2__ul">
                        <li class="footer-content__box2__li"><a class="footer-content__box2__a" href="#">Telecomms</a></li>
                        <li class="footer-content__box2__li"><a class="footer-content__box2__a" href="#">Travels</a></li>
                        <li class="footer-content__box2__li"><a class="footer-content__box2__a" href="#">Electronics</a></li>
                        <li class="footer-content__box2__li"><a class="footer-content__box2__a" href="#">Finance</a></li>
                        <li class="footer-content__box2__li"><a class="footer-content__box2__a" href="#">Hospitality</a></li>
                        <li class="footer-content__box2__li"><a class="footer-content__box2__a" href="#">Consumer Goods</a></li>
                    </ul>
                </div>
                <div class="footer-content__box3">
                    <h4 class="footer-content__box3__h4">Extras</h4>
                    <ul class="footer-content__box3__li">
                        <li class="footer-content__box3__li"><a class="footer-content__box3__a" href="#">About Offhill</a></li>
                        <li class="footer-content__box3__li"><a class="footer-content__box3__a" href="#">Contact Us</a></li>
                        <li class="footer-content__box3__li"><a class="footer-content__box3__a" href="#">Terms &amp; Conditions</a></li>
                        <li class="footer-content__box3__li"><a class="footer-content__box3__a" href="#">Privacy Policy</a></li>
                        <li class="footer-content__box3__li"><a class="footer-content__box3__a" href="#">Advertise</a></li>
                    </ul>
                </div>
            </div>

            <div class="footer-copyright">
                <p class="footer-copyright__p">
                    Copyright &copy; <?= date('Y'); ?>. All rights reserved.
                </p>
            </div>

        </div>
    </section>
</div>

<script src="./js/vue.js"></script>
<script src="./js/vue-resource.js"></script>
<script src="./js/app.js"></script>

</script>
</body>
</html>
