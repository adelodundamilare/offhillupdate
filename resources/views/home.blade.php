@extends('master')

@section('title', 'Welcome Page')

@section('body')
    <div class="slider">
        <div class="slider__form-div">
            <form action="#" class="slider__form">
                <h3 class="slider__form__h3">Find the Best <span class="slider__form__h3__span">Brands in Africa</span></h3>
                <div class="slider__form__input-box">
                    <input class="slider__form__input" type="text" placeholder="Enter Brand of Product's name">
                    <input class="slider__form__input" type="text" placeholder="Search for Issues or Complaints">
                    <button class="slider__form__button"><span class="slider__form__button__span">Search </span><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
                <p class="slider__form__p">Don't have an account? <a href="#" class="slider__form__p__a">Sign Up</a> here</p>
            </form>
        </div>
    </div>

    <!-- start the homepage top brand section -->
    <div class="top-brands">
        <div class="wrap">
            <div class="section-title section-padding">
                <span class="section-title__span"></span>
                <h2 class="section-title__h4">Top Brands</h2>
            </div>

            <div class="topBrand section-padding">
                <!-- topBrands component here -->
                @foreach ($brands->take(6) as $brand)

                <div class="topBrands">
                    <div class="topBrands__img">
                        <img src="{{ $brand->brand_logo }}" alt="{{ $brand->brand_name }}">
                    </div>
                    <div class="topBrands__desc">
                        <h4 class="topBrands__desc__title">{{ $brand->brand_name }}</h4>
                        <p class="topBrands__desc__text">{{ str_limit($brand->brand_about, 40, '...') }}</p>

                        <div class="topBrands__rating">
                            <span class="topBrands__rating__stars">

                                @foreach ($brand->rating as $stars):
                                <i class="fa fa-star topBrands__rating__stars-i" aria-hidden="true"></i>
                                @endforeach

                                <i class="fa fa-star-half-o topBrands__rating__stars-i" aria-hidden="true"></i>
                                <i class="fa fa-star-o topBrands__rating__stars-i" aria-hidden="true"></i>

                                <span class="topBrands__rating__numbers">
                                    {{ $brand->rating->sum('rating_star' 2 ) }}/5 from {{ $brand->rating->count() }} reviews
                                </span>
                            </span>
                            <button class="topBrands__rating__c2action">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add Review
                            </button>
                        </div>

                        <div class="topBrands__other-prod">

                            @if ($brand->prods->count() == 0)

                                <div class="topBrands__other-prod__div-sorry">
                                    Sorry! Brand has no product
                                </div>

                            @else
                                @foreach($brand->prods->take(2) as $products)
                                    <div class="topBrands__other-prod__div">
                                        <img src="{{ $products->product_img }}" alt="product_img" class="topBrands__other-prod__img">
                                        <h5 class="topBrands__other-prod__name"> {{ $products->product_name }} </h5>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>

                @endforeach
                <!-- topBrands component here -->
            </div>

        </div>
    </div>
    <!-- end the top brand homepage section -->

    <!-- homepage top issues here -->

    <div class="home-issues">
        <div class="wrap section-padding">
            <div class="section-title section-padding">
                <span class="section-title__span"></span>
                <h2 class="section-title__h4">Top Issues</h2>
                <p class="section-title__p">Find the topic consumers are talking about on our website</p>
            </div>

            <div class="home-issues__wrap">
                <div class="issues">
                    <div class="issues__box">
                        <div class="issues__box-first">
                            <ul class="issues__box-first__ul">
                                <li class="issues__box-first__li">
                                    <p class="issues__box-first__li-p">11</p>
                                    <span class="issues__box-first__li-span">votes</span>
                                </li>
                                <li class="issues__box-first__li">
                                    <p class="issues__box-first__li-p">5</p>
                                    <span class="issues__box-first__li-span">comments</span>
                                </li>
                                <li class="issues__box-first__li">
                                    <p class="issues__box-first__li-p">10</p>
                                    <span class="issues__box-first__li-span">views</span>
                                </li>
                            </ul>
                        </div>
                        <div class="issues__box-second">
                            <h4 class="issues__box-second__h4">Home basic cable and internet 424 s. Crest rd, chattanooga, TN</h4>
                            <p class="issues__box-second__p">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore</p>
                            <div class="issues__box-second__info">
                                <div class="issues__box-second__info-tag">
                                    <a href="#" class="issues__box-second__info-tag__a">hello</a>
                                    <a href="#" class="issues__box-second__info-tag__a">welcome</a>
                                    <a href="#" class="issues__box-second__info-tag__a">dummy</a>
                                </div>

                                <div class="issues__box-second__info-details">
                                    <p class="issues__box-second__info-details__p">added 6months ago</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="issues__box">
                        <div class="issues__box-first">
                            <ul class="issues__box-first__ul">
                                <li class="issues__box-first__li">
                                    <p class="issues__box-first__li-p">11</p>
                                    <span class="issues__box-first__li-span">votes</span>
                                </li>
                                <li class="issues__box-first__li">
                                    <p class="issues__box-first__li-p">5</p>
                                    <span class="issues__box-first__li-span">comments</span>
                                </li>
                                <li class="issues__box-first__li">
                                    <p class="issues__box-first__li-p">10</p>
                                    <span class="issues__box-first__li-span">views</span>
                                </li>
                            </ul>
                        </div>
                        <div class="issues__box-second">
                            <h4 class="issues__box-second__h4">Home basic cable and internet 424 s. Crest rd, chattanooga, TN</h4>
                            <p class="issues__box-second__p">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore</p>
                            <div class="issues__box-second__info">
                                <div class="issues__box-second__info-tag">
                                    <a href="#" class="issues__box-second__info-tag__a">hello</a>
                                    <a href="#" class="issues__box-second__info-tag__a">welcome</a>
                                    <a href="#" class="issues__box-second__info-tag__a">dummy</a>
                                </div>

                                <div class="issues__box-second__info-details">
                                    <p class="issues__box-second__info-details__p">added 6months ago</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="issues-sidebar">
                    this is a sidebar
                </div>
            </div>

            <div class="section-title section-padding">
                <h2 class="section-title__h4 section-title__h4-sm">Add new complaint!</h2>
                <p class="section-title__p section-title__p--foot">Click here to <button class="section-title__button"><i class="fa fa-plus" aria-hidden="true"></i> add new</button> complaint</p>
            </div>

        </div>
    </div>

    <!-- homepage top issues here too -->

    <!-- Trending Products, Latest Issues, Latest News -->
    <div class="three-column-section section-padding">
        <div class="wrap section-padding">
            <div class="three-column">
                <div class="three-column__div">
                    <div class="three-column__div-title">
                        <h3 class="three-column__div-h4">
                            Trending Products
                        </h3>
                        <span class="three-column__div-desc">
                            Get Latest News on Brands and their Products
                        </span>
                    </div>
                    <div class="trending-products">
                        <div class="trending-products__div">
                            <div class="trending-products__img">
                                <img src="{{ elixir('img/brands/tecno-smart.jpg') }}" alt="">
                            </div>
                            <div class="trending-products__content">
                                <h4 class="trending-products__content-name">Product Name</h4>
                                <span class="trending-products__content-category"><i class="fa fa-bookmark"></i> Telecomms</span>
                                <span class="topBrands__rating__stars">
                                    <i class="fa fa-star topBrands__rating__stars-i" aria-hidden="true"></i>
                                    <i class="fa fa-star topBrands__rating__stars-i" aria-hidden="true"></i>
                                    <i class="fa fa-star topBrands__rating__stars-i" aria-hidden="true"></i>
                                    <i class="fa fa-star topBrands__rating__stars-i" aria-hidden="true"></i>
                                    <i class="fa fa-star topBrands__rating__stars-i" aria-hidden="true"></i>
                                </span>
                                <span class="trending-products__content-reviews-number">
                                    reviewed by: 172 people
                                </span>
                            </div>
                        </div>
                        <div class="trending-products__div">
                            <div class="trending-products__img">
                                <img src="{{ elixir('img/brands/tecno-smart.jpg') }}" alt="">
                            </div>
                            <div class="trending-products__content">
                                <h4 class="trending-products__content-name">Product Name</h4>
                                <span class="trending-products__content-category"><i class="fa fa-bookmark"></i> Telecomms</span>
                                <span class="topBrands__rating__stars">
                                    <i class="fa fa-star topBrands__rating__stars-i" aria-hidden="true"></i>
                                    <i class="fa fa-star topBrands__rating__stars-i" aria-hidden="true"></i>
                                    <i class="fa fa-star topBrands__rating__stars-i" aria-hidden="true"></i>
                                    <i class="fa fa-star topBrands__rating__stars-i" aria-hidden="true"></i>
                                    <i class="fa fa-star topBrands__rating__stars-i" aria-hidden="true"></i>
                                </span>
                                <span class="trending-products__content-reviews-number">
                                    reviewed by: 172 people
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="three-column__div">
                    <div class="three-column__div-title">
                        <h3 class="three-column__div-h4">
                            Latest Issues
                        </h3>
                        <span class="three-column__div-desc">
                            Get Latest News on Brands and their Products
                        </span>
                    </div>
                    <div class="latest-issues">
                        <div class="latest-issues__div">
                            <h4 class="latest-issues__div-h4">This is the title here</h4>
                            <p class="latest-issues__div-p">
                                Added by
                                <a href="#" class="latest-issues__div-name">Alex Smith</a> <i class="fa fa-clock-o"></i> Today 2:40 pm - 24.06.2014</p>
                        </div>
                        <div class="latest-issues__div">
                            <h4 class="latest-issues__div-h4">This is the title here</h4>
                            <p class="latest-issues__div-p">
                                Added by
                                <a href="#" class="latest-issues__div-name">Alex Smith</a> <i class="fa fa-clock-o"></i> Today 2:40 pm - 24.06.2014</p>
                        </div>
                        <div class="latest-issues__div">
                            <h4 class="latest-issues__div-h4">This is the title here</h4>
                            <p class="latest-issues__div-p">
                                Added by
                                <a href="#" class="latest-issues__div-name">Alex Smith</a> <i class="fa fa-clock-o"></i> Today 2:40 pm - 24.06.2014</p>
                        </div>
                    </div>
                </div>
                <div class="three-column__div">
                    <div class="three-column__div-title">
                        <h3 class="three-column__div-h4">
                            Latest News
                        </h3>
                        <span class="three-column__div-desc">
                            Get Latest News on Brands and their Products
                        </span>
                    </div>
                    <div class="latest-news">
                        <div class="latest-news__div">
                            <div class="comments">
                                <img class="comments__img" src="{{ elixir('img/brands/tecno-smart.jpg') }}" alt="">
                                <div class="comments__body">
                                    <a href="#">Adelodun Damilare</a> rated <a href="#">GTBank's</a> brand
                                    <span class="comments__body__time">Today 2:10 pm - 12.06.2014</span>
                                    <div class="comments__reaction">
                                        <button class="comments__reaction__like"><i class="fa fa-thumbs-up"></i> Like</button>
                                        <button class="comments__reaction__read"><i class="fa fa-heart"></i> See Rating</button>
                                    </div>
                                </div>
                                <span class="comments__time">
                                    5mins ago
                                </span>
                            </div>
                            <div class="comments">
                                <img class="comments__img" src="{{ elixir('img/brands/tecno-smart.jpg') }}" alt="">
                                <div class="comments__body">
                                    <a href="#">Adelodun Damilare</a> added a new complain
                                    <span class="comments__body__time">Today 2:10 pm - 12.06.2014</span>
                                    <div class="comments__message-box">
                                        this is the message anyway, you can see more here...
                                    </div>
                                    <div class="comments__reaction">
                                        <button class="comments__reaction__like"><i class="fa fa-thumbs-up"></i> Like</button>
                                        <button class="comments__reaction__like"><i class="fa fa-thumbs-down"></i> Dislike</button>
                                        <button class="comments__reaction__write"><i class="fa fa-pencil"></i> Reply</button>
                                    </div>
                                </div>
                                <span class="comments__time">
                                    5mins ago
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Trending Products, Latest Issues, Latest News ends here -->

    <!-- join our community -->
    <div class="join-community section-padding">
        <div class="wrap section-padding">
            <div class="join-community__div zoomIn animated">
                <span class="join-community__icon"><i class="fa fa-comment big-icon"></i></span>
                <h2 class="join-community__h2">Join Our Community</h2>
                <span class="join-community__span">By creating an account, You agree to Offhill Inc. <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a></span>
                <div class="join-community__social">
                    <div class="join-community__social__gplus">
                        <i class="fa fa-google-plus"></i>
                        Sign Up with Google
                    </div>
                    <div class="join-community__social__facebook">
                        <i class="fa fa-facebook"></i>
                        Sign Up with Facebook
                    </div>
                </div>
                <span class="join-community__span">or you can <a href="#">Join</a> using your email address</span>
            </div>
        </div>
    </div>
    <!-- join our community -->

    <div class="featured-brands section-padding">
        <div class="wrap section-padding">
            <div class="featured-brands__div section-padding">
                <h3 class="featured-brands__div-h3">Featured Brands</h3>
                <span class="featured-brands__div-span">Brands featured today on our website</span>
                <div class="featured-brands__slider">
                    <img class="featured-brands__slider-img" src="{{ elixir('img/featured/chi.png') }}" alt="">
                    <img class="featured-brands__slider-img" src="{{ elixir('img/featured/cool-fm.jpg') }}" alt="">
                    <img class="featured-brands__slider-img" src="{{ elixir('img/featured/GLO.jpg') }}" alt="">
                    <img class="featured-brands__slider-img" src="{{ elixir('img/featured/guinness.jpg') }}" alt="">
                    <img class="featured-brands__slider-img" src="{{ elixir('img/featured/jumia-logo.jpg') }}" alt="">
                    <img class="featured-brands__slider-img" src="{{ elixir('img/featured/zenith-logo.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>

    <!-- Featured brands end here -->


    <!-- <h1><example></example></h1>
    <ul>
        <li><a href="#">this is the that</a></li>
        <li><a href="#">this is the list</a></li>
        <li>this is the list</li>
        <li>this is the list</li>
        <li>this is the list</li>
        <li>this is the list</li>
    </ul> -->

@endsection
