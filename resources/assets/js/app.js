
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

Vue.component('example', {
    template:
    `

    <div>
        <div class="container">
            Just Example!
        </div>
    </div>

    `
    ,
    mounted() {
        console.log('Component ready.')
    }
});

var app = new Vue({
    el: '#app',

    data: {
        show: false,
        showClass: "show-class",
        name: '',
        noDisplay: true
    },

    methods: {
        showMobileMenus: function() {
            return this.show = !this.show;
        },

        showChild(){
            return this.noDisplay = !this.noDisplay;
            this.noDisplay = true;
            console.log(showChild());
        }
    }
});

require('./test');
