<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    //
    public function index()
    {
        $brand = Brand::all();
        return view('brand.index', compact('brand'));
    }

}
