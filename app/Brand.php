<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    //
    public function prods()
    {
        return $this->hasMany('App\Products');
    }

    public function rating()
    {
        return $this->hasMany('App\Ratings');
    }
}
