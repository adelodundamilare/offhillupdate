<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    //
    public function brands()
    {
        return $this->belongsTo('App\Brand');
    }

    public function reviews(){
        return $this->hasMany('App\Reviews');
    }
}
