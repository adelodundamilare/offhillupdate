(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

Vue.component('example', {
    template: '\n\n    <div>\n        <div class="container">\n            Just Example!\n        </div>\n    </div>\n\n    ',

    mounted: function mounted() {
        console.log('Component ready.');
    }
});

var app = new Vue({
    el: '#app',

    data: {
        show: false,
        showClass: "show-class",
        name: '',
        noDisplay: true
    },

    methods: {
        showMobileMenus: function showMobileMenus() {
            return this.show = !this.show;
        },

        showChild: function (_showChild) {
            function showChild() {
                return _showChild.apply(this, arguments);
            }

            showChild.toString = function () {
                return _showChild.toString();
            };

            return showChild;
        }(function () {
            return this.noDisplay = !this.noDisplay;
            this.noDisplay = true;
            console.log(showChild());
        })
    }
});

require('./test');

},{"./test":2}],2:[function(require,module,exports){
'use strict';

console.log('Welcome sir!');

},{}]},{},[1]);

//# sourceMappingURL=app.js.map
